#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include <time.h>  

#define GAME_SCREEN    1
#define RECORDS_SCREEN 2
#define ABOUT_SCREEN   3 
#define MENU_SCREEN    4

int launch_main_menu(void) 
{
  int parent_x, parent_y, new_x, new_y;
  int score_size = 1;
  int current_item = 1;
  int ch;
  time_t clk;
  char menu_list[3][10] = {"Start", "Records", "About"};

  // set up initial windows
  getmaxyx(stdscr, parent_y, parent_x);

  WINDOW* field = newwin(parent_y - score_size, parent_x, 0, 0);
  WINDOW* score = newwin(score_size, parent_x, parent_y - score_size, 0);

  keypad(field, true);
  box(field, 0, 0);

  init_pair(2, COLOR_BLACK, COLOR_GREEN);
  wbkgd(score, COLOR_PAIR(2));

  while (ch != 10) {
    getmaxyx(stdscr, new_y, new_x);

    if (new_y != parent_y || new_x != parent_x) {
      parent_x = new_x;
      parent_y = new_y;

      wresize(field, new_y - score_size, new_x);
      wresize(score, score_size, new_x);
      mvwin(score, new_y - score_size, 0);

      wclear(stdscr);
      wclear(field);
      wclear(score);

      box(field, 0, 0);
    }

    // draw to our windows
    for (int i = 1; i <= 3; i++) {
        if (i == current_item) 
          wattron(field, A_STANDOUT ); // highlights the first item.
        else
          wattroff(field, A_STANDOUT );
        mvwprintw(field, 
                  i - 1 + ((new_y - score_size)  / 2), 
                  2 + (new_x / 2), "%s", menu_list[i-1]);
    }

    mvwprintw(score, 0, 0, "Score");
    time(&clk);
    mvwprintw(score, 0, new_x - 20,"%s", ctime(&clk));

    // refresh each window
    wrefresh(field);
    wrefresh(score);

    ch = wgetch(field);
    switch(ch) {
      case 'k': {
        current_item--;
        if (current_item == 0)
          current_item = 2;
        break;
      }
      case 'j': {
        current_item++;
        if (current_item == 4)
          current_item = 1;
        break;
      }
      case KEY_UP: {
        current_item--;
        if (current_item == 0)
          current_item = 2;
        break;
      }
      case KEY_DOWN: {
        current_item++;
        if (current_item == 4)
          current_item = 1;
        break;
      }
      case 'q': {
        delwin(field);
        delwin(score);
        return EXIT_SUCCESS;
      }
      default:
        break;
    }
  }

  delwin(field);
  delwin(score);

  return current_item;
}

int launch_about_menu(void) 
{
  int current_item;
  return current_item;
}


int main(int argc, char *argv[]) 
{
  int current_screen = MENU_SCREEN;
  int res = -1; 

  initscr();
  start_color();
  noecho();
  curs_set(FALSE);

  while (res != EXIT_SUCCESS) {
    if (current_screen == MENU_SCREEN)
      res = launch_main_menu();
    else if (current_screen == GAME_SCREEN)
      printf("%d ", res);
      // do smth
    else if (current_screen == RECORDS_SCREEN)
      printf("%d ", res);
      // do smth
    else
      printf("%d ", res);
      // do smth
    printf("%d ", res);
  } 
  endwin();

  return res;
}
